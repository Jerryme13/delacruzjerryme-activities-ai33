var ctx = document.getElementById('bar').getContext('2d');
var myChart = new Chart(ctx, {
    type: 'bar',
    data: {
        labels: ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL','AUG','SEPT','OCT','NOV','DEC',],
        datasets: [{
            label: 'Borrowed Books, Returned Books',
            data: [49, 50, 60, 20, 39, 70, 77,99,65,66,56,54,],
            backgroundColor: ['blue','orange','blue','orange','blue','orange','blue','orange','blue','orange','blue','orange',],
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});


var pie = document.getElementById('pie').getContext('2d');
var myChart = new Chart(pie, {
    type: 'pie',
    data: {
        labels: ['SCIENCE','ENGLISH','FILIPINO','MATH'],
        datasets: [{
            label: '# of Votes',
            data: [30, 8, 7, 5],
            backgroundColor: ['blue','orange','gray','yellow',],
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});



